/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ebay;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jfpal
 */
public class Categoria {
    String codigo;
    String nombre;
    ArrayList<SubCat> subcats;

    public Categoria(String codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.subcats =new ArrayList<SubCat>();
    }

    public Categoria() {
        this.codigo = "";
        this.nombre = "";
        this.subcats =new ArrayList<SubCat>();
    }

    @Override
    public String toString() {
        return "Categoria{" + "codigo=" + codigo + ", nombre=" + nombre + ", subcats=" + subcats + '}';
    }
    
    
    
    
}
