/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ebay;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.*;  
import javax.mail.internet.*;  
import javax.activation.*;  
import javax.mail.Session;

/**
 *
 * @author jfpal
 */
public class Ebay {
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
        
        float ebayGanancias;
        ArrayList<Categoria> Categorias = new ArrayList<Categoria>();
        
        ArrayList<Usuario> usuarios = new ArrayList<>();
        ArrayList<Anuncio> anuncios = new ArrayList<>();
        ArrayList<OrdenCompra> ordenes = new ArrayList<>();
        
        readCsv(Categorias);
        
        Scanner in = new Scanner(System.in);
        int op = 0;
        while (op != 3) {
            
            portadaEbay();
            Opciones1();
            op = in.nextInt();
            in.nextLine(); 
            if (op == 1) {
                if (!usuarios.isEmpty()) {
                    portadaEbay();
                    String cu;
                    String pass;
                    Usuario u1;
                    System.out.println("              Iniciar Sesion.");
                    while (true) {
                        System.out.println("Correo o usuario: ");
                        cu = in.nextLine();
                        System.out.println("Contraseña: ");
                        pass = in.nextLine();
                        u1=checkPass(usuarios, cu, cu, pass);
                        if (u1!=null) {
                            break;
                        }
                    }
                    
                    int op2=0;
                    while (op2!=4) { 
                        portadaEbay();
                        menuUser();
                        op2 = in.nextInt();
                        if (op2==1) {
                            if (!anuncios.isEmpty()) {
                                System.out.println(anuncios);
                            }
                            else{
                                System.out.println("No existen anuncios. Agregue un anuncio");
                            }
                            
                        }
                        if (op2==2) {
                            if (u1.getIsVendedor()) {
                                menuAnuncio(u1, anuncios,Categorias);
                            }
                            else{
                                System.out.println("No es anunciante.Ingrese en opciones para activar esta opcion");
                            }
                        }
                        if (op2==3) {
                            int op3=0;
                            while (op3!=4) {
                                portadaEbay();
                                menuOpciones();
                                op3=in.nextInt();
                                in.nextLine(); 
                                if (op3==1) {
                                    int op4=0;
                                    portadaEbay();
                                    askAnunciante(u1);
                                    
                                 
                                }
                                if (op3==2) {
                                    portadaEbay();
                                    menuDir2(u1);
                                    
                                }
                                if (op3==3) {
                                    portadaEbay();
                                    menuTipo(u1);
                                    
                                    
                                }
                            }
                            
                        }
                    }
                } else {
                    System.out.println("No se puede iniciar sesion debido a que no existen usuarios. Crear un usuario.");
                }
            }
            if (op == 2) {
                EmailValidator ev = new EmailValidator();
                Usuario us=null;
                String mail;
                String user;
                boolean validEmail;
                portadaEbay();
                
                while (true){              
                    
                    System.out.println("Correo: ");
                    while (true) {
                        
                        mail = in.nextLine();
                        validEmail=ev.validate(mail);
                        if (validEmail) {
                            break;
                        }
                        
                        

                    }
                    System.out.println("Username: ");
                    user = in.nextLine();
                    
                    us = new Usuario(user, mail);
                    
                    if (!checkUser(usuarios, mail, user))
                        break;
                    
                    
                }
                usuarios.add(us);
                String pass;
                
                System.out.println("Contraseña: ");
                while (true) {                    
                    pass= in.nextLine();
                    if (pass.length()==6) {
                        break;
                    }
                }
                us.setPass(pass);
            
            }
        }

    }

    public static void portadaEbay() {
        System.out.println("============================================");
        System.out.println("                  Ebay.com");
        System.out.println("============================================");
    }

    public static void Opciones1() {
        System.out.println("              1. Iniciar Sesion.");
        System.out.println("              2. Regsitrarse.");
        System.out.println("              3. Salir.");
        System.out.println("============================================");
        System.out.println("Opcion: ");
    }
    public static void askAnunciante(Usuario u1) {
        
         Scanner in = new Scanner(System.in);
        int op4=0;
        if (!u1.getIsVendedor()) {
            System.out.println("           Quieres ser anunciante?");
            System.out.println("                  1. Si");
            System.out.println("                  2. No");
            System.out.println("============================================");
            System.out.println("Opcion: ");
            op4=in.nextInt();
            if (op4==1) {
                u1.setIsVendedor(true);
            }
        }
        else{
            System.out.println("Usted ya es anunciante...");
        }
        
        
    }
    
    public static void menuUser(){
        System.out.println("              1. Ver anuncios.");
        System.out.println("              2. Anunciar");
        System.out.println("              3. Opciones.");
        System.out.println("              4. Regresar");
        System.out.println("============================================");
        System.out.println("Opcion: ");
    }
    public static void menuOpciones(){
        System.out.println("              1. Quieres ser anunciante?.");
        System.out.println("              2. Direccion de envio.");
        System.out.println("              3. Tipo de pago");
        System.out.println("              4. Regresar");
        System.out.println("============================================");
        System.out.println("Opcion: ");
    }
    public static void menuDir1(Usuario u){
        String str=u.getDireccion();
        System.out.println("  Direccion actual: "+str);
        System.out.println("    Desea cambiar su direccion actual?");
        System.out.println("                1. Si");
        System.out.println("                2. No");
        System.out.println("============================================");
        System.out.println("Opcion: ");
    }
    public static void menuDir2(Usuario u1){
        Scanner in = new Scanner(System.in);
        if ("".equals(u1.getDireccion())) {
            System.out.println("Ingresar Direccion: ");
            String dir = in.nextLine();
            u1.setDireccion(dir);
        }
        else{
            int op5=0;
            menuDir1(u1);
            op5=in.nextInt();
            in.nextLine(); 
            if (op5==1) {
                String dir = in.nextLine();
                u1.setDireccion(dir);
            }
        }
    }
    public static void menuTipo(Usuario u1){
        Scanner in = new Scanner(System.in);
        if (u1.getTipo()!=4) {
            int op6=0;
            if(u1.getTipo()==1)
                    System.out.println("Efectivo");
            if(u1.getTipo()==2)
                    System.out.println("Paypal");
            if(u1.getTipo()==3)
                    System.out.println("Tarjeta de Credito");
            
            System.out.println("Desea cambiar de tipo de pago?");
            System.out.println("         1. Si");
            System.out.println("         2. No");
            System.out.println("Opcion: ");
            op6=in.nextInt();
            if (op6==1) {
                int op7=0;
                System.out.println("Elegir tipo:");
                System.out.println("1. Efectivo");
                System.out.println("2. Paypal");
                System.out.println("3. Tarjeta de Credito");
                System.out.println("Opcion: ");
                op7=in.nextInt();
                u1.setTipo(op7);
            }
        }
        else{
            int op7=0;
            System.out.println("Elegir tipo:");
            System.out.println("1. Efectivo");
            System.out.println("2. Paypal");
            System.out.println("3. Tarjeta de Credito");
            System.out.println("Opcion: ");
            op7=in.nextInt();
            u1.setTipo(op7);
        }
    }
            ArrayList<Anuncio> anuncios=new ArrayList<Anuncio>();

    public static ArrayList<Anuncio> getAnuncioOfUser(ArrayList<Anuncio> anun,Usuario u){
        ArrayList<Anuncio> anuncios=new ArrayList<Anuncio>();
        for (int i=0;i<anun.size();i++){
            if (anun.get(i).getUser()==u) {
                
                anuncios.add(anun.get(i));
            }
            
        }
        return anuncios;
    }
    
    public static void menuAnuncio(Usuario u1,ArrayList<Anuncio> anun,ArrayList<Categoria> cats){
        portadaEbay();
        Scanner in = new Scanner(System.in);
        ArrayList<Anuncio> anuncios=getAnuncioOfUser(anun, u1);
        if (anuncios.isEmpty()) {
            Anuncio a=null;
            System.out.println("Titulo");
            String tit=in.nextLine();
            System.out.println("Descripcion");
            String desc=in.nextLine();
            System.out.println("Elegir condicion de producto: ");
            System.out.println("1. Nuevo");
            System.out.println("2. Usado");
            System.out.println("3. No funciona");
            System.out.println("Opcion: ");
            int op=in.nextInt();
            System.out.println("Ingresar el precio: ");
            float precio=in.nextFloat();
            System.out.println("Ingresar el Stock: ");
            int stock=in.nextInt();
            String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
            System.out.println("Elegir Categoria:");
            printCat(cats);
            System.out.println("Opcion: ");
            int opcat=in.nextInt();
            Categoria c=cats.get(opcat-1);
            System.out.println("Elegir Subcategoria:");
            printSubcat(c);
            System.out.println("Opcion: ");
            int opSubcat=in.nextInt(); 
            a=new Anuncio(tit, desc, op, true, precio, stock, date,u1,c.subcats.get(opSubcat-1).codigo);
            int opPago=0;
            while (opPago!=2) {                
                System.out.println("Elegir tipo de pago:");
                System.out.println("1. Efectivo");
                System.out.println("2. Paypal");
                System.out.println("3. Tarjeta de Credito");
                System.out.println("Opcion:");
                int tipo=in.nextInt();
                a.addTipoPago(tipo);
                System.out.println("Desea agregar otro tipo de pago?");
                System.out.println("1. Si");
                System.out.println("2. No");
                opPago=in.nextInt();
                
            }
            int opEntrega=0;
            while (opEntrega!=2) {                
                System.out.println("Elegir tipo entrega: ");
                System.out.println("1. Local");
                System.out.println("2. Correo nacional.");
                System.out.println("3. Correo Internacional");
                System.out.println("Opcion: ");
                int tipo=in.nextInt();
                a.addTipoEntrega(tipo);
                System.out.println("Desea agregar otro tipo de Entrega?");
                System.out.println("1. Si");
                System.out.println("2. No");
                opEntrega=in.nextInt();

            } 
            
            
            anun.add(a);
        }
        else{
            int op=0;
            while (op!=3) {                
                System.out.println("    Anuncios");
                System.out.println("1. Crear");
                System.out.println("2. Editar");
                System.out.println("3. Regresar");
                System.out.println("Opcion:");
                op=in.nextInt();
                if (op==1) {
                    Anuncio a=null;
                    System.out.println("Titulo");
                    String tit=in.nextLine();
                    System.out.println("Descripcion");
                    String desc=in.nextLine();
                    System.out.println("Elegir condicion de producto: ");
                    System.out.println("1. Nuevo");
                    System.out.println("2. Usado");
                    System.out.println("3. No funciona");
                    System.out.println("Opcion: ");
                    int op2=in.nextInt();
                    
                    System.out.println("Ingresar el precio: ");
                    float precio=in.nextFloat();
                    System.out.println("Ingresar el Stock: ");
                    int stock=in.nextInt();
                    String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
                    System.out.println("Elegir Categoria:");
                    printCat(cats);
                    System.out.println("Opcion: ");
                    int opcat=in.nextInt();
                    Categoria c=cats.get(opcat-1);
                    System.out.println("Elegir Subcategoria:");
                    printSubcat(c);
                    System.out.println("Opcion: ");
                    int opSubcat=in.nextInt(); 
                    a=new Anuncio(tit, desc, op, true, precio, stock, date,u1,c.subcats.get(opSubcat-1).codigo);
                    int opPago=0;
                    while (opPago!=2) {                
                        System.out.println("Elegir tipo de pago:");
                        System.out.println("1. Efectivo");
                        System.out.println("2. Paypal");
                        System.out.println("3. Tarjeta de Credito");
                        System.out.println("Opcion:");
                        int tipo=in.nextInt();
                        a.addTipoPago(tipo);
                        System.out.println("Desea agregar otro tipo de pago?");
                        System.out.println("1. Si");
                        System.out.println("2. No");
                        opPago=in.nextInt();

                    } 
                    int opEntrega=0;
                    while (opEntrega!=2) {                
                        System.out.println("Elegir tipo entrega: ");
                        System.out.println("1. Local");
                        System.out.println("2. Correo nacional.");
                        System.out.println("3. Correo Internacional");
                        System.out.println("Opcion: ");
                        int tipo=in.nextInt();
                        a.addTipoEntrega(tipo);
                        System.out.println("Desea agregar otro tipo de Entrega?");
                        System.out.println("1. Si");
                        System.out.println("2. No");
                        opEntrega=in.nextInt();

                    } 


                    anun.add(a);
                    
                }
                if (op==2) {
                    int op4=0;
                    Anuncio a=null;
                    for (int i = 0; i < anun.size(); i++) {
                        if (anun.get(i).getUser().getUsername().equals(u1.getUsername())) {
                            System.out.println((i+1) +". "+anun.get(i));
                        }
                        
                    }
                    op4=in.nextInt();
                    for (int i = 0; i < anun.size(); i++) {
                        if (anun.get(i).getUser().getUsername().equals(u1.getUsername())) {
                            if (i==op4-1) {
                                a=anun.get(i);
                            }
                           
                        }
                        
                    }
                    
                    System.out.println("Titulo");
                    String tit=in.nextLine();
                    a.setTitulo(tit);
                    System.out.println("Descripcion");
                    String desc=in.nextLine();
                    a.setDes(desc);
                    System.out.println("Elegir condicion de producto: ");
                    System.out.println("1. Nuevo");
                    System.out.println("2. Usado");
                    System.out.println("3. No funciona");
                    System.out.println("Opcion: ");
                    int op2=in.nextInt();
                    a.setCond(op2);
                    System.out.println("Ingresar el precio: ");
                    float precio=in.nextFloat();
                    a.setPrecio(precio);
                    System.out.println("Ingresar el Stock: ");
                    int stock=in.nextInt();
                    a.setStock(stock);
                    //String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
                    System.out.println("Elegir Categoria:");
                    printCat(cats);
                    System.out.println("Opcion: ");
                    int opcat=in.nextInt();
                    Categoria c=cats.get(opcat-1);
                    System.out.println("Elegir Subcategoria:");
                    printSubcat(c);
                    System.out.println("Opcion: ");
                    int opSubcat=in.nextInt(); 
                    a.setCodCat(c.subcats.get(opSubcat-1).codigo);
                    int opPago=0;
                    while (opPago!=2) {                
                        System.out.println("Elegir tipo de pago:");
                        System.out.println("1. Efectivo");
                        System.out.println("2. Paypal");
                        System.out.println("3. Tarjeta de Credito");
                        System.out.println("Opcion:");
                        int tipo=in.nextInt();
                        a.addTipoPago(tipo);
                        System.out.println("Desea agregar otro tipo de pago?");
                        System.out.println("1. Si");
                        System.out.println("2. No");
                        opPago=in.nextInt();

                    }
                    int opEntrega=0;
                    while (opEntrega!=2) {                
                        System.out.println("Elegir tipo entrega: ");
                        System.out.println("1. Local");
                        System.out.println("2. Correo nacional.");
                        System.out.println("3. Correo Internacional");
                        System.out.println("Opcion: ");
                        int tipo=in.nextInt();
                        a.addTipoEntrega(tipo);
                        System.out.println("Desea agregar otro tipo de Entrega?");
                        System.out.println("1. Si");
                        System.out.println("2. No");
                        opEntrega=in.nextInt();

                    } 
                }
                
            }
            
            
            
            
            
        }
        
        
        
        
    }
    
    
    
    public static boolean checkUser(ArrayList<Usuario> us,String mail,String user){
        for (int i=0;i<us.size();i++){
            
            if (us.get(i).getMail().equals(mail)&& us.get(i).getUsername().equals(user)){
                return true;
            }
            
        }
        return false;
    }
    
    public static Usuario checkPass(ArrayList<Usuario> us,String mail,String user, String pass){
        Usuario u=null;
        for (int i=0;i<us.size();i++){
            
            if (us.get(i).getMail().equals(mail)|| us.get(i).getUsername().equals(user)&&us.get(i).getPass().equals(pass)){
                return us.get(i);
            }
        }
        return null;
    }
    
    public static void printCat(ArrayList<Categoria> cats){
        for (int i = 0; i < cats.size(); i++) {
            System.out.println((i+1) + ". "+cats.get(i).nombre);
        }
    }
    public static void printSubcat(Categoria cat) {
        for (int i = 0; i < cat.subcats.size(); i++) {
            System.out.println((i+1) + ". "+cat.subcats.get(i).nombre);
        }
    }
    
    public static void readCsv(ArrayList<Categoria> cats){
         //Input file which needs to be parsed
        String fileToParse = "categorias.csv";
        BufferedReader fileReader = null;
         
        //Delimiter used in CSV file
        final String DELIMITER = ",";
        try
        {
            String line = "";
           int code=0;
           fileReader = new BufferedReader(new FileReader(fileToParse));
           Categoria c =null;
             
            //Read the file line by line
            while ((line = fileReader.readLine()) != null) 
            {
                
                String[] tokens = line.split(DELIMITER);
                //System.out.println("NEW: "+tokens);
                
                if (code==0){
                    code=Integer.parseInt(tokens[0]);
                    c=new Categoria();
                }
                if (code==Integer.parseInt(tokens[0])) {
                    c.codigo=tokens[0];
                    c.nombre=tokens[1];
                    SubCat sb=null;
                    if (c.subcats.size()<10) {
                        sb = new SubCat(tokens[0]+"0"+(c.subcats.size()+1), tokens[2]);
                    }
                    else{
                        sb = new SubCat(tokens[0]+(c.subcats.size()+1), tokens[2]);
                    }
                    c.subcats.add(sb);

                }
                else{
                    cats.add(c);
                    code=Integer.parseInt(tokens[0]);
                    c=new Categoria();
                    c.codigo=tokens[0];
                    c.nombre=tokens[1];
                    SubCat sb=null;
                    if (c.subcats.size()<10) {
                        sb = new SubCat(tokens[0]+"0"+(c.subcats.size()+1), tokens[2]);
                    }
                    else{
                        sb = new SubCat(tokens[0]+(c.subcats.size()+1), tokens[2]);
                    }
                    c.subcats.add(sb);
                    
                }
            }
        } 
        catch (Exception e) {
            e.printStackTrace();
        } 
        finally
        {
            try {
                fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    public static void printCats(ArrayList<Categoria> cats){
        for (int i = 0; i < cats.size(); i++) {
            System.out.println(cats.get(i));
        }
    }
    
    public void searchAnuncio(ArrayList<Categoria> cats, ArrayList<Anuncio> anuns,Usuario u,ArrayList<OrdenCompra> ordenes){
        Scanner in = new Scanner(System.in);
        String to = "";//change accordingly  
        String from = "rockerlp23@gmail.com";//change accordingly  
        String host = "localhost";//or IP address 
        
        Properties properties = System.getProperties();  
        properties.setProperty("mail.smtp.host", host);  
        Session session = Session.getDefaultInstance(properties);
        
        int op=in.nextInt();
        while (op!=3) {            
            System.out.println("Buscar Anuncio");
            System.out.println("1. Por Categoria.");
            System.out.println("2. Por Titulo de Anuncio.");
            System.out.println("3. Regresar");
            System.out.println("Opcion: ");
            op=in.nextInt();
            if (op==1) {
                System.out.println("Elegir Categoria:");
                printCat(cats);
                System.out.println("Opcion: ");
                int opcat=in.nextInt();
                Categoria c=cats.get(opcat-1);
                System.out.println("Elegir Subcategoria:");
                printSubcat(c);
                System.out.println("Opcion: ");
                int opSubcat=in.nextInt(); 
                System.out.println("Que articulo desea comprar?");
                ArrayList<Anuncio> anunByCat=getListAnunBySubcat(anuns, getCodigoSubcat(c, opSubcat));
                printAnuncios(anunByCat);
                int opAnun=in.nextInt(); 
                
                Anuncio a = anunByCat.get(opAnun-1);
                System.out.println(a);
                System.out.println("Desea comprar este producto?");
                System.out.println("1. Si");
                System.out.println("2. No");
                int opcomprar=in.nextInt(); 
                switch (opcomprar){
                    case 1:
                        
                        if(a.getTipos().contains(u.getTipo() )){
                            
                            //Date t;
                            SimpleDateFormat ft = new SimpleDateFormat ("dd-MM-yyyy"); 
                            
                            Calendar cal = Calendar.getInstance();
                            System.out.println("Cuantos desea?");
                            int cant=in.nextInt();
                            
                            
                            System.out.println("Elegir tipo de pago: ");
                            a.getTipoPago();
                            int tipoPago=in.nextInt();
                            String tipoPagoString = a.getTipoPagoByParam(tipoPago);
                            
                            a.getTipoEntrega();
                            int op3=in.nextInt();
                            String tipoEntregaString=a.getTipoEntregaByParam(op3);
                            String Date = null;
                            switch(op3){
                                case 1:
                                    Date=ft.format(cal.getTime());
                                case 2:
                                    cal.add(Calendar.DATE, 3);
                                    Date=ft.format(cal.getTime());
                                case 3:
                                    cal.add(Calendar.DATE, 4);
                                    Date=ft.format(cal.getTime());
                            }
                            to=u.getMail();
                            OrdenCompra oc=new OrdenCompra(new SimpleDateFormat("dd-MM-yyyy").format(new Date()), a.getTitulo(), cant, Date, 1, tipoPagoString,tipoEntregaString);
                            ordenes.add(oc);
                            try{
                                MimeMessage message = new MimeMessage(session);
                                message.setFrom(new InternetAddress(from));
                                message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));
                                message.setSubject("Compra de: "+a.getTitulo());
                                message.setText("Usted ha comprado el producto: "+a.getTitulo()+"\nDescripcion: "+a.getDes()+"\nPrecio: "+a.getPrecio()+"\nPago: "+oc.getTipoPago()+"\nTipo entrega: "+oc.getTipoEntrega()+"Fecha estimada de entrega: "+oc.getFechaEntrega());

                                // Send message
                                Transport.send(message);
                                System.out.println("message sent successfully....");

                             }catch (MessagingException mex) {mex.printStackTrace();}
                            
                        }
                        else{
                            System.out.println("El usuario: "+a.getUser().getUsername()+" no acepta el tipo de pago: "+u.getTipoPago());
                        }
                }
            }
            if (op==2) {
                
            }
            if (op==3) {
                break;
            }
            
        }
        
        
        
    }
    public String getCodigoSubcat(Categoria cat,int subCat){
        String cod="";
        for (int i = 0; i < cat.subcats.size(); i++) {
            if (i==subCat-1) {
                return cod=cat.subcats.get(i).codigo;
            }
            
        }
        return null;
    }
    public ArrayList<Anuncio> getListAnunBySubcat(ArrayList<Anuncio> anuns,String subCat){
        ArrayList<Anuncio> a = new ArrayList<>();
        for (int i = 0; i < anuns.size(); i++) {
            if (anuns.get(i).getCodCat().equals(subCat) && anuns.get(i).getEstado()) {
                a.add(anuns.get(i));
            }
        }
        return a;
        
    }
    public void printAnuncios(ArrayList<Anuncio> anuns){
        for (int i = 0; i < anuns.size(); i++) {
            if (anuns.get(i).getEstado()) {
                System.out.println((i+1)+"Titulo: "+anuns.get(i).getTitulo()+" Precio: "+anuns.get(i).getPrecio());
                System.out.println(" Tipo de pago: ");
                anuns.get(i).getTipoPago();
            }
            
        }
    }
    
    
    
}
