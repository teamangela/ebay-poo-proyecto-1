/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ebay;

import java.util.ArrayList;

/**
 *
 * @author jfpal
 */
public class Anuncio {
    private String titulo;
    private String des;
    private int cond;
    private boolean estado;
    
    private float precio;
    private int stock;
    private String date;
    private ArrayList<String> tiposPago;
    private ArrayList<String> tiposEntrega;
    private String codCat;
    private Usuario user;

    public Anuncio(String titulo, String des,int cond, boolean estado, float precio, int stock, String date, Usuario us,String codCat) {
        this.titulo = titulo;
        this.des = des;
        this.cond=cond;
        this.estado = estado;
        this.tiposEntrega= new ArrayList<>();
        this.precio = precio;
        this.stock = stock;
        this.date = date;
        this.tiposPago= new ArrayList<>();
        this.user=us;
        this.codCat=codCat;
    }

    public Anuncio() {
    }

    public String getCodCat() {
        return codCat;
    }

    public void setCodCat(String codCat) {
        this.codCat = codCat;
    }
    
    

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }
    
    

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public ArrayList<String> getTiposPago() {
        return tiposPago;
    }

    public void setTiposPago(ArrayList<String> tiposPago) {
        this.tiposPago = tiposPago;
    }

    public ArrayList<String> getTiposEntrega() {
        return tiposEntrega;
    }

    public void setTiposEntrega(ArrayList<String> tiposEntrega) {
        this.tiposEntrega = tiposEntrega;
    }

    

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<String> getTipos() {
        return tiposPago;
    }

    public void setTipos(ArrayList<String> tipos) {
        this.tiposPago = tipos;
    }

   

    public int getCond() {
        return cond;
    }

    public void setCond(int cond) {
        this.cond = cond;
    }

    @Override
    public String toString() {
        return "Titulo: " + titulo + "\nDescripcion: " + des + "\nCondicion: " + cond + "\nEstado: " + estado + "\nPrecio: " + precio + "\nStock: " + stock + "\nFecha:" + date + "\nUsuario: " + user.getUsername();
    }
    
    public void addTipoPago(int tipo){
        switch (tipo) {
            case 1:
                if (!this.tiposPago.contains("Efectivo")) {
                    this.tiposPago.add("Efectivo");
                }
                else{
                    System.out.println("Tipo de pago ya seleccionado...");
                }
            case 2:    
                if (!this.tiposPago.contains("Paypal")) {
                    this.tiposPago.add("Paypal");
                }
                else{
                    System.out.println("Tipo de pago ya seleccionado...");
                }
            case 3:
                if (!this.tiposPago.contains("Tarjeta de Credito")) {
                    this.tiposPago.add("Tarjeta de Credito");
                }
                else{
                    System.out.println("Tipo de pago ya seleccionado...");
                }
                
        }
    }
    public void getTipoPago(){
        for (int i = 0; i < this.tiposPago.size(); i++) {
            System.out.println((i+1)+". "+this.tiposPago.get(i));
        }
    }
    
    public void getTipoEntrega(){
        for (int i = 0; i < this.tiposEntrega.size(); i++) {
            System.out.println((i+1)+". "+this.tiposEntrega.get(i));
        }
    }
    
    public String getTipoPagoByParam(int opTipo){
        for (int i = 0; i < this.tiposPago.size(); i++) {
            if (i==opTipo) {
                return this.tiposPago.get(i);
            }
            
        }
        return null;
    }
    public String getTipoEntregaByParam(int opTipo){
        for (int i = 0; i < this.tiposEntrega.size(); i++) {
            if (i==opTipo) {
                return this.tiposEntrega.get(i);
            }
            
        }
        return null;
    }
    
    public void addTipoEntrega(int tipo){
        switch (tipo) {
            case 1:
                if (!this.tiposPago.contains("Local")) {
                    this.tiposPago.add("Local");
                }
                else{
                    System.out.println("Tipo de Entrega ya seleccionado...");
                }
            case 2:    
                if (!this.tiposPago.contains("Correo nacional")) {
                    this.tiposPago.add("Correo nacional");
                }
                else{
                    System.out.println("Tipo de Entrega ya seleccionado...");
                }
            case 3:
                if (!this.tiposPago.contains("Correo Internacional")) {
                    this.tiposPago.add("Correo Internacional");
                }
                else{
                    System.out.println("Tipo de Entrega ya seleccionado...");
                }
                
        }
    }
    
}
