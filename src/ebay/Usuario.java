/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ebay;

/**
 *
 * @author jfpal
 */
public class Usuario {
    private String username;
    private String pass;
    private String mail;
    private Boolean isVendedor;
    private String direccion;
    private int tipo;

    public Usuario(String username, String pass, String mail, Boolean isVendedor, String direccion, int tipo) {
        this.username = username;
        this.pass = pass;
        this.mail = mail;
        this.isVendedor = isVendedor;
        this.direccion = direccion;
        this.tipo = tipo;
    }

    public Usuario(String username, String mail) {
        this.username = username;
        this.mail = mail;
        this.isVendedor = false;
        this.direccion = "";
        this.tipo = 4;
    }
    
    

    public Usuario() {
    }
    
    

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Boolean getIsVendedor() {
        return isVendedor;
    }

    public void setIsVendedor(Boolean isVendedor) {
        this.isVendedor = isVendedor;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
    
    public String getTipoPago(){
        switch (this.tipo) {
            case 1:
                return "Efectivo";
            case 2:       
                return "Paypal";
            case 3:
                return "Tarjeta de Credito";
        }
        return "No existe tipo de pago";
    }
}
