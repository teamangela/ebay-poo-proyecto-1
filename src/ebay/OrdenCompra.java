/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ebay;

/**
 *
 * @author jfpal
 */
public class OrdenCompra {
    private String fecha;
    private String producto;
    private int cant;
    private String fechaEntrega;
    private int estado;
    private String tipoPago;
    private String tipoEntrega;

    public OrdenCompra(String fecha, String producto, int cant, String fechaEntrega, int estado, String tipoPago, String entrega) {
        this.fecha = fecha;
        this.producto = producto;
        this.cant = cant;
        this.fechaEntrega = fechaEntrega;
        this.estado = estado;
        this.tipoPago = tipoPago;
        this.tipoEntrega = entrega;
    }
    

    

    public OrdenCompra() {
    }

    
    
    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public int getCant() {
        return cant;
    }

    public void setCant(int cant) {
        this.cant = cant;
    }

    public String getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    public String getTipoEntrega() {
        return tipoEntrega;
    }

    public void setTipoEntrega(String tipoEntrega) {
        this.tipoEntrega = tipoEntrega;
    }

    
    
    
    
}
