/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ebay;

/**
 *
 * @author jfpal
 */
public class SubCat {
    String codigo;
    String nombre;

    public SubCat(String codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public SubCat() {
        this.codigo = "";
        this.nombre = "";
    }

    @Override
    public String toString() {
        return "Codigo: " + codigo + " Nombre: " + nombre + '}';
    }
    
    
}
